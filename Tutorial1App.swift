//
//  Tutorial1App.swift
//  Shared
//
//  Created by Gabor Simonic on 27/02/2022.
//

import SwiftUI

@main
struct Tutorial1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
